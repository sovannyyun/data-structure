/*
Author:					Sovanny Yun
Date created:			09/30/15
Last Modification Date:	10/03/15
File Name:				Datastructor_assignment1
Source:					Turtors

Overview:
	This program will allow users to create an array with
	a certain data type that has a certain length and a starting 
	index.
*/
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "array.h"
#include "Exception.h"

#include <iostream>

using namespace std;

void dumpMemory()
{
	Array<int> defaultConstructor;
	Array<int> intArray(5, -1);
	try
	{
		intArray[-1] = 4;
		intArray[0] = 42;
		intArray[3] = 26;
 		//intArray[7] = 2; //Exception is thrown.

		int start_ID = intArray.getStartIndex();
		int len = intArray.getLength();

		cout << "Start index: " << start_ID << std::endl;
		cout << "Length: " << len << std::endl;
	}
	catch (Exception& exc)
	{
		cout << "Exception: " << exc.getMessage() << std::endl;
	}
}

int main()
{
	dumpMemory();

	_CrtDumpMemoryLeaks();
	return 0;
}