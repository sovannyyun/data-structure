#include "Exception.h"

template <typename T>
void Array<T>::renewArray(T* arr, const int& len)
{
	if (len == 0 || arr == nullptr)
		throw Exception("Invalid arguments.");

	if (m_array) //if not empty, delete it
		delete [] m_array;

	m_array = new T[len]; // allocating new size of old array
	if (!m_array)
		throw Exception("Could not allocate memory.");

	for (int i = 0; i < len; ++i)
		m_array[i] = arr[i];
}

//Defualt constructor constructs and reserves space for these 3 obj
template <typename T>
Array<T>::Array()	:
	m_array(nullptr),
	m_length(0),
	m_start_index(0)
{

}

//This func creates length and start_index for new array
template <typename T>
Array<T>::Array(const int& length, const int& start_index = 0)	:
	m_start_index(start_index)
{
	if (length <= 0)
		throw Exception("invalid lenght.");

	m_length = length;

	m_array = new T[m_length]; // allocating new size of old array
	if (!m_array)
		throw Exception("Could not allocate memory.");

	for (int i = 0; i < m_length; ++i)
		m_array[i] = 0;
}

//Copy constuctor copies what is on left to rhs
template <typename T>
Array<T>::Array(const Array<T>& rhs)
{
	m_length = rhs.m_length;
	m_start_index = rhs.m_start_index
	renewArray(rhs.m_array, rhs.m_lenght);
}

//Destructor deletes array and set everything to 0
template <typename T>
Array<T>::~Array()
{
	if (m_array)
		delete [] m_array;

	m_length = 0;
	m_start_index = 0;
}

template <typename T>
int Array<T>::getStartIndex() const 
{ 
	return m_start_index; 
}

template <typename T>
void Array<T>::setStartIndex(const int& start_index)
{
	m_start_index = start_index;
}

template <typename T>
int Array<T>::getLength() const
{
	return m_length;
}

template <typename T>
void Array<T>::setLength(const int& length)
{
	if (length == 0 || length >= m_length)
		throw Exception("Length is invalid.");

	m_lenght = length;
}

// Checking for self-assignment to avoid infinite loop
template <typename T>
Array<T>& Array<T>::operator= (const Array<T>& rhs)
{
	if (this == &rhs)
		return *this;

	renewArray(rhs.m_array, rhs.m_length);
	return *this;
}

// Make sure index is within the range
template <typename T>
T& Array<T>::operator[](const int& index)
{
	if (index < m_start_index || index > (m_start_index + m_length - 1))
		throw Exception("invalid index.");

	return m_array[index - m_start_index];
}