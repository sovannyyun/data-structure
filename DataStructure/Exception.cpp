
#include "Exception.h"


ostream& operator<<(ostream& os, const Exception& rhs)
{
	os << rhs.m_msg;
	return (os);
}

// Default constructor ==> not taking any
Exception::Exception()	:
	m_msg(nullptr) // initialize to null pointer
{

}

Exception::Exception(const char* msg)
{
	if (strlen(msg) == 0 || msg == nullptr)
		return;

	m_msg = new char[strlen(msg) + 1]; // allocating new size of old array
	if (!m_msg)
		return;

	for (int i = 0; i < (int) strlen(msg); ++i)
		m_msg[i] = msg[i];

	m_msg[strlen(msg)] = '\0'; // make sure last character is NULL	
}

Exception::Exception(const Exception& rhs)
{
	renewCharArray(rhs.m_msg); // copy whatever on right to left hand side
}							   // This is copy constructor, not assignment

Exception::~Exception()
{
	if (m_msg)
		delete [] m_msg;
}

const char* Exception::getMessage() const
{
	return m_msg;
}

void Exception::setMessage(const char* msg)
{
	renewCharArray(msg);
}

// just make sure that allocated memory is not leaked
void Exception::renewCharArray(const char* arr)
{
	if (strlen(arr) == 0 || arr == nullptr)
		return;

 	if (m_msg)		   //if not empty, delete it
		delete [] m_msg;

	m_msg = new char[strlen(arr) + 1]; // allocating new size of old array
	if (!m_msg)
		return;

	strcpy_s(m_msg, strlen(m_msg), arr);

	m_msg[strlen(arr)] = '\0'; // make sure last character is NULL
}

Exception& Exception::operator=(const Exception &rhs)
{
	if (this == &rhs)
		return *this;

	renewCharArray(rhs.m_msg);
	return *this;
}
