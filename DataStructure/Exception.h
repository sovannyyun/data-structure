
#ifndef EXCEPTION
#define EXCEPTION

#include <iostream>
using namespace std;

/*
	Represents an instance of Exception
*/
class Exception 
{
public:
	/*
		Default constructor
	*/
	Exception();

	/*
		Instantiates a new instance of Exception
		@param msg The exception message
	*/
	Exception(const char* msg);

	/*
		Instantiates a new instance of Exception from one instance of Exception
		@param rhs Another instance of Exception 
	*/
	Exception(const Exception& rhs);

	/*
		Destructor
	*/
	~Exception(); 

	/*
		Gets the undrelying message of Exception
		@returns The underlying message
	*/
	const char* getMessage() const;

	/*
		Sets the underlying message of Exception
		@param msg The message to be set.
	*/
	void setMessage(const char* msg); 

	/*
		Assignment operator
		@param rhs The instance of Excetption.
		@returns this instance of Exception.
	*/
	Exception& operator=(const Exception &rhs);

	/*
		Insertion operator
		@param os The output stream.
		@param rhs The Exception instance.
		@returns The output stream.
	*/
	friend ostream& operator<<(ostream& os, const Exception& rhs); 

private:
	/*
		The underlying Exception message
	*/
	char* m_msg;

	/*
		Renew and assign new values for a char array.
		@param arr The new value.
	*/
	void renewCharArray(const char* arr);
};

#endif