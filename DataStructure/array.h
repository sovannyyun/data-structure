
#ifndef ARRAY_H
#define ARRAY_H

template <typename T>
class Array
{
public:
	Array(); //Default constructor
	Array(const int& length, const int& start_index = 0); //Constructor
	Array(const Array& rhs); //Copy constructor
	~Array(); //Destructor

	//This func get and return index of array
	int getStartIndex() const; 

	//This func set "start_index" to member variable "m_start_index"
	void setStartIndex(const int& start_index); 

	//This func get and return the length of array
	int getLength() const;

	// This func set "length" to member variable "m_length"
	void setLength(const int& length);

	//Assignment operator assigns what is on left to right hand side
	Array& operator=(const Array& rhs);

	//Subscript operator uses to access 1 element of array
	T& operator[] (const int& index); // same as arr[2].

private:
	T* m_array;
	int m_length;
	int m_start_index;

	void renewArray(T* arr, const int& len);
};

#include "array.inc"
#endif